package com.asymmetrik.mobile.keyboard;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class RadixTreeAutocompleteProviderTest {
    @Test
    public void emptyFragmentResultsInEmptyCandidates() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("foo bar");
        Assert.assertTrue(toTest.getWords("").isEmpty());
    }

    @Test
    public void nullFragmentResultsInEmptyCandidates() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("foo bar");
        Assert.assertTrue(toTest.getWords(null).isEmpty());
    }

    @Test
    public void emptyInputIsIgnored() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("");
        Assert.assertTrue(toTest.getWords("f").isEmpty());
    }

    @Test
    public void strangeInputIsIgnored() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("foo . - / \\ , [ ] { } : ; ' \" ! @ # $ % ^ & * ( ) ~ ` foos");
        Set<Candidate> expected = Sets.newHashSet(
                new Candidate("foos", 1),
                new Candidate("foo", 1));
        Assert.assertEquals(expected, new HashSet<>(toTest.getWords("foo")));
    }

    @Test
    public void providedExample1() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("The third thing that I need to tell you is that this thing does not think thoroughly.");
        Set<Candidate> expected = Sets.newHashSet(
                new Candidate("thing", 2),
                new Candidate("think", 1),
                new Candidate("third", 1),
                new Candidate("this", 1));
        Assert.assertEquals(expected, new HashSet<>(toTest.getWords("thi")));
    }

    @Test
    public void providedExample2() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("The third thing that I need to tell you is that this thing does not think thoroughly.");
        Set<Candidate> expected = Sets.newHashSet(new Candidate("need", 1));
        Assert.assertEquals(expected, new HashSet<>(toTest.getWords("nee")));
    }

    @Test
    public void providedExample3() {
        RadixTreeAutocompleteProvider toTest = new RadixTreeAutocompleteProvider();
        toTest.train("The third thing that I need to tell you is that this thing does not think thoroughly.");
        Set<Candidate> expected = Sets.newHashSet(
                new Candidate("that", 2),
                new Candidate("thing", 2),
                new Candidate("think", 1),
                new Candidate("this", 1),
                new Candidate("third", 1),
                new Candidate("the", 1),
                new Candidate("thoroughly", 1));
        Assert.assertEquals(expected, new HashSet<>(toTest.getWords("th")));
    }
}
