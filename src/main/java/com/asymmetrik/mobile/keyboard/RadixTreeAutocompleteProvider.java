package com.asymmetrik.mobile.keyboard;

import com.google.common.base.Strings;
import com.googlecode.concurrenttrees.radix.ConcurrentRadixTree;
import com.googlecode.concurrenttrees.radix.node.concrete.SmartArrayBasedNodeFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Implementation for {@link AutocompleteProvider} backed by a {@link ConcurrentRadixTree}.
 */
public class RadixTreeAutocompleteProvider extends AutocompleteProvider {
    private final ConcurrentRadixTree<Integer> data;

    public RadixTreeAutocompleteProvider() {
        this.data = new ConcurrentRadixTree<>(new SmartArrayBasedNodeFactory());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Candidate> candidatesFor(String fragment) {
        return Candidate.allFrom(data.getKeyValuePairsForKeysStartingWith(fragment));
    }

    /**
     * Provides content to be used to help calculate {@link Candidate}s for using
     * {@link #getWords(String)}.
     * <p>
     * The {@code passage} is split-up using
     * {@code passage.split("\\W+")} and each resulting word is converted to lower-case.
     *
     * @param passage content used for training.  Can contain characters within UTF-8 or UTF-16
     */
    @Override
    public void train(String passage) {
        Arrays.stream(passage.split("\\W+"))
                .parallel()
                .filter(s -> !Strings.isNullOrEmpty(s))
                .map(s -> s.toLowerCase())
                .forEach(this::putOrIncrement);
    }

    /**
     * Synchronization here is to ensure that concurrent calls will block to ensure the count
     * increments atomically.  The locking here will slow down training, but is necessary.
     */
    private synchronized void putOrIncrement(String s) {
        Integer count = data.getValueForExactKey(s);
        if (count == null || count == 0) {
            data.put(s, 1);
        } else {
            data.put(s, count + 1);
        }
    }
}
