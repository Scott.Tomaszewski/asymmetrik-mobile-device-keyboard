package com.asymmetrik.mobile.keyboard;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Service used for predicting candidates for autocompletion given a prefix.
 */
public abstract class AutocompleteProvider {
    /**
     * Calculates potential autocompletion candidates
     *
     * @param fragment prefix to autocomplete.  Can be {@code null} or empty.
     * @return list of potential candidates the {@code fragment} can be completed as if
     * {@code fragment} is not {@code null} nor empty.  Otherwise returns and empty list
     */
    public List<Candidate> getWords(String fragment) {
        return Strings.isNullOrEmpty(fragment) ? new ArrayList<>() : candidatesFor(fragment);
    }

    /**
     * Provides content to be used to help calculate {@link Candidate}s for using
     * {@link #getWords(String)}.
     *
     * @param passage content used for training.  Can contain characters within UTF-8 or UTF-16
     */
    public abstract void train(String passage);

    /**
     * Calculates potential autocompletion candidates
     *
     * @param fragment prefix to autocomplete. Will never be null or empty.
     * @return list of potential candidates the {@code fragment} can be completed as.
     */
    abstract List<Candidate> candidatesFor(String fragment);
}
