package com.asymmetrik.mobile.keyboard;

import com.google.common.base.Objects;
import com.googlecode.concurrenttrees.common.KeyValuePair;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

/**
 * Container for autocomplete results.
 */
public class Candidate {
    public static List<Candidate> allFrom(Iterable<KeyValuePair<Integer>> wordToCount) {
        return stream(wordToCount.spliterator(), false)
                .map(Candidate::from)
                .sorted(confidenceComparator().reversed())
                .collect(toList());
    }


    public static Candidate from(KeyValuePair<Integer> wordToCount) {
        return new Candidate(wordToCount.getKey(), wordToCount.getValue());
    }

    private final CharSequence word;
    private final Integer confidence;

    public Candidate(CharSequence word, Integer confidence) {
        this.word = word;
        this.confidence = confidence;
    }

    public String getWord() {
        return word.toString();
    }

    public int getConfidence() {
        return confidence;
    }

    @Override
    public String toString() {
        return String.format("%s (%d)", getWord(), getConfidence());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object != null && object instanceof Candidate) {
            Candidate that = (Candidate) object;
            return Objects.equal(this.word, that.word)
                    && Objects.equal(this.confidence, that.confidence);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.word, this.confidence);
    }

    /**
     * @return a {@link Comparator} for comparing {@link Candidate}s based on their
     * {@link #getConfidence()}.
     */
    private static Comparator<? super Candidate> confidenceComparator() {
        return (first, second) -> first.confidence.compareTo(second.confidence);
    }
}
