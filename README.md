## Asymmetrik Mobile Device Keyboard Autocompletion

Service used for predicting candidates for autocompletion given a prefix.  Only provided
implementation uses a Concurrent Radix Tree for space and lookup efficiency while providing
concurrent access for usage in an online manner.  The Tree nodes are considered "smart" and will
initially be setup to use UTF-8 for space efficiency, but will switch to UTF-16 should the need
arise.

This code is provided under the MIT License.

## Build

This is a standard maven project which you can run with `mvn clean install`

## Example usage

```
public static void main(String[] args) {
    RadixTreeAutocompleteProvider autocompletion = new RadixTreeAutocompleteProvider();

    autocompletion.train("The third thing that I need to tell you is that this thing does not think thoroughly.");
    System.out.println(autocompletion.getWords("th"));

    autocompletion.train("This is the threat that thrives through thirst.");
    System.out.println(autocompletion.getWords("th"));
}
```

Which will output

```
[that (2), thing (2), the (1), think (1), third (1), this (1), thoroughly (1)]
[that (3), the (2), thing (2), this (2), think (1), third (1), thirst (1), thoroughly (1), threat (1), thrives (1), through (1)]
```

## Future features

- Add support for words with punctuation (hyphens, for example)
- Add support for capitalization

## Acknowledgments

- [Concurrent-Trees library](https://github.com/npgall/concurrent-trees)